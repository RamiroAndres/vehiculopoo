﻿using UnityEngine;
using System.Collections;

public class ruedas : MonoBehaviour {

	// Use this for initialization
	public void crear () {
		GameObject rueda1;
		GameObject rueda2;
		GameObject rueda3;
		GameObject rueda4;
		rueda1 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		rueda1.name = "rueda1";
		rueda1.transform.position = new Vector3 (1.6f, 0, 1.2f);
		rueda1.transform.localScale = new Vector3 (1, 1 , 0.3f);
		rueda2 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		rueda2.name = "rueda2";
		rueda2.transform.position = new Vector3 (1.6f, 0, -1.2f);
		rueda2.transform.localScale = new Vector3 (1, 1 , 0.3f);
		rueda3 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		rueda3.name = "rueda3";
		rueda3.transform.position = new Vector3 (-1.6f, 0, 1.2f);
		rueda3.transform.localScale = new Vector3 (1, 1 , 0.3f);
		rueda4 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		rueda4.name = "rueda4";
		rueda4.transform.position = new Vector3 (-1.6f, 0, -1.2f);
		rueda4.transform.localScale = new Vector3 (1, 1 , 0.3f);
	}

}
