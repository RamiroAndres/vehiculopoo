﻿using UnityEngine;
using System.Collections;

public class ventanas : MonoBehaviour {

	// Use this for initialization
	public void crear () {
		GameObject ventanaIzquierda;
		GameObject ventanaDerecha;
		GameObject ventanaPrincipal;
		GameObject ventanaTracera;
		ventanaDerecha = GameObject.CreatePrimitive (PrimitiveType.Plane);
		ventanaDerecha.name = "ventanaDerecha";
		ventanaDerecha.transform.position = new Vector3 (0.2f, 0.85f, 1.36f);
		ventanaDerecha.transform.rotation = Quaternion.Euler (90, 0, 0);
		ventanaDerecha.transform.localScale = new Vector3 (0.26f, 0, 0.05f);
		ventanaIzquierda = GameObject.CreatePrimitive (PrimitiveType.Plane);
		ventanaIzquierda.name = "ventanaIzquierda";
		ventanaIzquierda.transform.position = new Vector3 (0.2f, 0.85f, -1.36f);
		ventanaIzquierda.transform.rotation = Quaternion.Euler (270, 0, 0);
		ventanaIzquierda.transform.localScale = new Vector3 (0.26f, 0, 0.05f);
		ventanaPrincipal = GameObject.CreatePrimitive (PrimitiveType.Cube);
		ventanaPrincipal.name = "ventanaPrincipal";
		ventanaPrincipal.transform.position = new Vector3 (-1.27f, 0.55f, 0);
		ventanaPrincipal.transform.rotation = Quaternion.Euler (0, 0, 45);
		ventanaPrincipal.transform.localScale = new Vector3 (1, 0.5f, 2.7f);
		ventanaTracera = GameObject.CreatePrimitive (PrimitiveType.Cube);
		ventanaTracera.name = "ventanaTracera";
		ventanaTracera.transform.position = new Vector3 (1.68f, 0.55f, 0);
		ventanaTracera.transform.rotation = Quaternion.Euler (0, 0, 135);
		ventanaTracera.transform.localScale = new Vector3 (1, 0.5f, 2.7f);
	}

}
