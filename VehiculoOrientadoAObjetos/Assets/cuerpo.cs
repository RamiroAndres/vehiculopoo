﻿using UnityEngine;
using System.Collections;

public class  cuerpo : MonoBehaviour {

	// Use this for initialization
	public void crear () {
		GameObject centro = new GameObject ();
		centro = GameObject.CreatePrimitive (PrimitiveType.Cube);
		centro.name = "Centro";
		centro.transform.position = new Vector3(0,0.3f,0);
		centro.transform.localScale = new Vector3 (5, 0.6f, 3);
		GameObject cola = new GameObject ();
		cola = GameObject.CreatePrimitive (PrimitiveType.Plane);
		cola.name = "Cola";
		cola.transform.position = new Vector3 (2.76f, 0.3f, 0);
		cola.transform.rotation = Quaternion.Euler(50, 90, 0);
		cola.transform.localScale = new Vector3 (0.3f, 0, 0.08f);
		GameObject cuerpoArriba = new GameObject ();
		cuerpoArriba = GameObject.CreatePrimitive (PrimitiveType.Cube);
		cuerpoArriba.name="cuerpoArriba";
		cuerpoArriba.transform.position = new Vector3 (0.2f, 0.85f, 0);
		cuerpoArriba.transform.localScale = new Vector3 (2.6f, 0.5f, 2.7f);
		GameObject frente = new GameObject ();
		frente = GameObject.CreatePrimitive (PrimitiveType.Plane);
		frente.name = "frente";
		frente.transform.position = new Vector3 (-2.78f, 0.3f, 0);
		frente.transform.rotation = Quaternion.Euler (310, 90, 0);
		frente.transform.localScale = new Vector3 (0.3f, 0 , 0.08f);
		GameObject parrila = new GameObject ();
		parrila = GameObject.CreatePrimitive (PrimitiveType.Plane);
		parrila.name = "parrila";
		parrila.transform.position = new Vector3 (-2.9f, 0.2f, 0);
		parrila.transform.rotation = Quaternion.Euler (310, 90, 0);
		parrila.transform.localScale = new Vector3 (0.15f, 0 , 0.04f);
		GameObject plano = new GameObject ();
		plano = GameObject.CreatePrimitive (PrimitiveType.Plane);
		plano.name = "plano";
		plano.transform.localScale = new Vector3 (0.6f, 1, 0.3f);
	}
}
