﻿using UnityEngine;
using System.Collections;

public class luz : MonoBehaviour {

	// Use this for initialization
	public void crear () {
		GameObject luz = new GameObject ("Luz");
		luz.AddComponent<Light> ();
		luz.light.type = LightType.Directional;
		luz.light.color = Color.white;
		luz.transform.position = new Vector3 (0, 15, -15);
		luz.transform.rotation = Quaternion.Euler(45, 0, 0);
	}

}
