﻿using UnityEngine;
using System.Collections;

public class placas : MonoBehaviour {
	// Use this for initialization
	public void crear () {
		GameObject placaFrente;
		GameObject placaAtras;
		placaFrente = GameObject.CreatePrimitive (PrimitiveType.Plane);
		placaFrente.name = "placaFrente";
		placaFrente.transform.position = new Vector3 (-3, 0.12f, 0);
		placaFrente.transform.rotation = Quaternion.Euler (310, 90, 0);
		placaFrente.transform.localScale = new Vector3 (0.04f, 100 , 0.02f);
		placaAtras = GameObject.CreatePrimitive (PrimitiveType.Plane);
		placaAtras.name = "placaAtras";
		placaAtras.transform.position = new Vector3 (3, 0.12f, 0);
		placaAtras.transform.rotation = Quaternion.Euler (50, 90, 0);
		placaAtras.transform.localScale = new Vector3 (0.04f, 100 , 0.02f);	
	}

}
