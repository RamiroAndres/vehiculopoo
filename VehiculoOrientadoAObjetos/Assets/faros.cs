﻿using UnityEngine;
using System.Collections;

public class faros : MonoBehaviour {

	// Use this for initialization
	public void crear () {
		GameObject farolDerecha;
		GameObject farolIzquierda;
		GameObject farolAtrasDerecha;
		GameObject farolAtrasIzquierda;
		farolDerecha = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		farolDerecha.name = "farolDerecha";
		farolDerecha.transform.position = new Vector3 (-2.8f, 0.28f, 1);
		farolDerecha.transform.rotation = Quaternion.Euler (0, 0, 320);
		farolDerecha.transform.localScale = new Vector3 (0, 0.3f, 0.3f);
		farolIzquierda = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		farolIzquierda.name = "farolIzquierda";
		farolIzquierda.transform.position = new Vector3 (-2.8f, 0.28f, -1);
		farolIzquierda.transform.rotation = Quaternion.Euler (0, 0, 320);
		farolIzquierda.transform.localScale = new Vector3 (0, 0.3f, 0.3f);
		farolAtrasDerecha = GameObject.CreatePrimitive (PrimitiveType.Plane);
		farolAtrasDerecha.name = "farolAtrasDerecha";
		farolAtrasDerecha.transform.position = new Vector3 (2.8f, 0.27f, 1.1f);
		farolAtrasDerecha.transform.rotation = Quaternion.Euler (0, 0, 310);
		farolAtrasDerecha.transform.localScale = new Vector3 (0.03f , 100 , 0.06f);
		farolAtrasIzquierda = GameObject.CreatePrimitive (PrimitiveType.Plane);
		farolAtrasIzquierda.name = "farolAtrasIzquierda";
		farolAtrasIzquierda.transform.position = new Vector3 (2.8f, 0.27f, -1.1f);
		farolAtrasIzquierda.transform.rotation = Quaternion.Euler (0, 0, 310);
		farolAtrasIzquierda.transform.localScale = new Vector3 (0.03f, 100 , 0.06f);		
	}

}
